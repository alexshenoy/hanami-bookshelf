Hanami::Model.migration do
  change do
    create_table :authors do
      primary_key :id

      column :name, String, null: false

      column :created_at, DateTime, null: false
      column :updated_at, DateTime, null: false
    end

    alter_table :books do
      drop_column :author
      add_foreign_key :author_id, :authors, on_delete: :cascade
    end
  end
end
