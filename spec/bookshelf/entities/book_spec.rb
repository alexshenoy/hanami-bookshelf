require_relative '../../spec_helper'

describe Book do
  it 'can be initialized with attrs' do
    book = Book.new(title: 'Some book')
    book.title.must_equal 'Some book'
  end
end
