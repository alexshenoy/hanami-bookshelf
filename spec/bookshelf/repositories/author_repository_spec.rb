require_relative '../../spec_helper'

describe AuthorRepository do
  let(:author_repository) { AuthorRepository.new }
  let(:book_repository) { BookRepository.new }

  before do
    @sandi = author_repository.create(name: 'Sandi Metz')

    @poodr = book_repository.create(title: 'POODR', author_id: @sandi.id)
    @bottles = book_repository.create(title: '99 Bottle of OOP', author_id: @sandi.id)
  end

  describe '#find_with_books' do
    it 'returns an author with aggregate books' do
      author = author_repository.find_with_books(@sandi.id)
      author.id.wont_be_nil
      author.books.must_equal [@poodr, @bottles]
    end
  end
  # place your tests here
end
