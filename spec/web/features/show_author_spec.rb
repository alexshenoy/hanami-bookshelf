require 'features_helper'

describe 'Show author' do
  let(:author_repository) { AuthorRepository.new }
  let(:book_repository) { BookRepository.new }

  before do
    author_repository.clear
    book_repository.clear

    @sandi = author_repository.create(name: 'Sandi Metz')
    @poodr = book_repository.create(title: 'POODR', author_id: @sandi.id)
    @bottles = book_repository.create(title: '99 Bottles Of OOP', author_id: @sandi.id)
  end

  it 'displays an author and all their books' do
    visit "/authors/#{@sandi.id}"

    within '#author' do
      assert page.has_content?('Sandi Metz')
    end

    within '#books' do
      assert page.has_css?('.book', count: 2), 'Expected Sandi to have two books'
      assert page.has_content?('POODR')
      assert page.has_content?('99 Bottles Of OOP')
    end
  end
end
