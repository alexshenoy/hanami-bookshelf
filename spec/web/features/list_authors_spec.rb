require 'features_helper'

describe 'List authors' do
  let(:author_repository) { AuthorRepository.new }

  before do
    author_repository.clear

    author_repository.create(name: 'Sandi Metz')
    author_repository.create(name: 'Avdi Grimm')
  end

  it 'shows a list of authors' do
    visit '/authors'

    within '#authors' do
      assert page.has_css?('.author', count: 2), 'Expected to find 2 two authors'
      assert page.has_content?('Sandi Metz')
      assert page.has_content?('Avdi Grimm')
    end
  end
end
