require_relative '../../../spec_helper'

describe Web::Controllers::Authors::Index do
  let(:action) { Web::Controllers::Authors::Index.new }
  let(:params) { Hash[] }
  let(:author_repository) { AuthorRepository.new }

  before do
    author_repository.clear

    @martin = author_repository.create(name: 'Martin Fowler')
    @dhh = author_repository.create(name: 'DHH')
  end

  it 'is successful' do
    response = action.call(params)
    response[0].must_equal 200
  end

  it 'exposes all authors' do
    action.call(params)
    action.exposures[:authors].must_equal [@martin, @dhh]
  end
end
