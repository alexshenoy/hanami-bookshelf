require_relative '../../../spec_helper'

describe Web::Controllers::Authors::Show do
  let(:action) { Web::Controllers::Authors::Show.new }
  let(:author_repository) { AuthorRepository.new }
  let(:book_repository) { BookRepository.new }

  before do
    author_repository.clear
    book_repository.clear

    @sandi = author_repository.create(name: 'Sandi Metz')
    @poodr = book_repository.create(title: 'POODER', author_id: @sandi.id)
    @bottles = book_repository.create(title: '99 Bottles Of OOP', author_id: @sandi.id)
  end

  describe 'with valid params' do
    let(:params) { Hash[id: @sandi.id] }

    it 'is successful' do
      response = action.call(params)
      response[0].must_equal 200
    end

    it 'exposes author with books' do
      action.call(params)
      action.exposures[:author].must_equal @sandi
      action.exposures[:author].books.must_equal [@poodr, @bottles]
    end
  end
end
