class BookRepository < Hanami::Repository

  associations do
    belongs_to :author
  end

  def create_with_author(params)
    author = AuthorRepository.new.create(name: params.dig(:author))
    book = create(title: params.dig(:title), author_id: author.id)
    aggregate(:author).where(id: book.id).map_to(Book).one
  end

  def all_with_authors
    aggregate(:author).map_to(Book).to_a
  end
end
